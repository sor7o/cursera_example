const mongoose = require('mongoose');
const Bike = require('../../models/bikes');
const User = require('../../models/user');
const Reserve = require('../../models/reserve');

describe('Testing', function () {
    beforeAll(function (done) {
        mongoose.connection.close().then(() => {
            const mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function () {
                console.log('Connected to database test');
                done();
            });
        });
    });
    afterEach(function (done) {
        Reserve.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            User.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Bike.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });
    describe('User makes a reservation', () => {
        it('Exist the reservation', (done) => {
            const user = new User({ name: 'Angel' });
            user.save();
            const bike = new Bike({ codigo: 1, color: "green", model: "ruban" });
            bike.save();
            const today = new Date();
            const tomorrow = new Date();
            tomorrow.setDate(today.getDate() + 1);
            user.reserves(bike.id, today, tomorrow, function (err, reserve) {
                const ne = Reserve.find({}).populate('bike').populate('user');
                console.log(ne[0]);
                Reserve.find({}).populate('bike').populate('user').exec(function(err, reserves){ 
                     console.log(reserves);
                    done();
                });
            });
        });
    });
});