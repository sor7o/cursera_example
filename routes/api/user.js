const express= require('express');
const router =express.Router();
const userControllerApi= require('../../controllers/api/userControllerAPI');

router.get('/', userControllerApi.user_list);
router.post('/create', userControllerApi.user_create);
router.post('/reserve', userControllerApi.user_reserve);
router.post('/update',userControllerApi.update_reserve);
router.post('/update_user',userControllerApi.update_user);
module.exports =router;