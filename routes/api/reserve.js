const express= require('express');
const router = express.Router();
const reserveController = require('../../controllers/api/reserveController');


router.get('/', reserveController.reserve_list);
router.post('/update',reserveController.reserve_update);

module.exports= router;