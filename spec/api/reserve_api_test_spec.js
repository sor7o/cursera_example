const mongoose = require('mongoose');
const Bikes = require('../../models/bikes');
const server = require('../../bin/www');
const request = require('request');
const Reserve = require('../../models/reserve');
const User = require('../../models/user');

const url_base = "http://localhost:3000/api/reserve";

describe('Reserve API', () => {
    beforeEach(function (done) {
        mongoose.connection.close().then(() => {
            const mongoDB = 'mongodb://localhost/red_bikes';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function () {
                console.log('Connected to database');
                done();
            });
        });
    });
    afterEach(function (done) {
        Reserve.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });
    describe('GET list reserve', () => {
        it('List reserve', (done) => {
            request.get(url_base, function (error, response, body) {
                const result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.length).toBe(0);
                done();
            })
        })
    });
    describe('POST /update', () => {
        it('update reserve existing', (done) => {
            const header = { 'content-type': 'application/json' };

            const user = new User({ "name": "Angel Sorto" });
            user.save();
            const bike = new Bikes({ codigo: 21, "color": "blue", "model": "urban" });
            bike.save();
            console.log(bike);
            const reserve = new Reserve({
                from: "2020-12-15",
                to: "2020-12-18",
                id: user._id,
                bike: bike._id

            });
            console.log(reserve);
            reserve.save();
            const updateReserve = {
                _id: reserve._id,
                from: "2020-12-18",
                to: "2020-12-21",
                bike: bike._id
            };
            console.log(updateReserve);
            request.post({
                headers: header,
                url: url_base + '/update',
                body: JSON.stringify(updateReserve)
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                const reserveSuccess = JSON.parse(body);
                expect(reserveSuccess.ok).toBe(1);
                done();
            })
        })
    })
})

