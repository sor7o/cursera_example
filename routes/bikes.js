var express = require('express');
var router = express.Router();
var bikesControllers = require('../controllers/bikes');

router.get('/', bikesControllers.list_bikes);

router.get('/create', bikesControllers.create);

router.post('/create',bikesControllers.createBike);

router.post('/:codigo/delete', bikesControllers.deleteBike);

router.get('/:codigo/update', bikesControllers.update);

router.post('/:codigo/update', bikesControllers.updateBike);


module.exports = router;