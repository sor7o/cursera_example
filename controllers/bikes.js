var Bikes = require('../models/bikes');

exports.list_bikes= function(req, res){
    res.render('bikes/index', {bikes : Bikes.allBikes});
};

exports.create = function(req, res){
    res.render('bikes/create');
};

exports.createBike = function(req, res){
    var bike = new Bikes(req.body.codigo, req.body.color, req.body.model);
    bike.ubication= [req.body.latitude, req.body.longitude];
    Bikes.add(bike);

    res.redirect('/bikes')
};

exports.deleteBike = function(req, res){
    Bikes.removeById(req.body.codigo);

    res.redirect('/bikes');
};

exports.update = function(req, res){
    var bike = Bikes.findById(req.params.codigo);
    res.render('bikes/update',{bike})
};
exports.updateBike= function(req, res){
    var bike = Bikes.findById(req.params.codigo);
    bike.id=req.body.codigo;
    bike.color= req.body.color;
    bike.model=req.body.model;    
    bike.ubication= [req.body.latitude, req.body.longitude];
    res.redirect('/bikes')
};