const express = require('express');
const router = express.Router();
const authController = require('../../controllers/api/sessionControllerAPI');
const passport = require('../../config/passport');
router.post('/authenticate', authController.authenticate);
router.post('/reset_password', authController.forgotPassword);
router.get('/password_reset/:token', authController.reset_password);
router.post('/reset/:token', authController.update_password);
router.post('/facebook_token', passport.authenticate('facebook-token', { scope: ["email", "id", "displayName", "picture"]}), authController.authFacebookToken);

module.exports =router;