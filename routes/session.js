const express= require('express');
const passport = require('passport');
const router= express.Router();
const sessionController = require('../controllers/session');


router.get('/', sessionController.login);
router.post('/',sessionController.post_login);
router.get('/reset_password', sessionController.reset_pass);
router.post('/reset_password', sessionController.reset_password);
router.get('/password_reset/:token', sessionController.pass_reset);
router.post('/password_reset', sessionController.password_reset);
router.get('/logout', sessionController.logout);
module.exports = router;
