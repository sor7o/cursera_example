const User = require('../models/user');
const Token = require('../models/token');

module.exports = {
    confirmationGet : function(req, res, next){
        Token.findOne({token: req.params.token}, function(err, token){
            if(!token) return res.status(400).send({types: 'not-verified', msg:'Not exist'})
            User.findById(token._userId, function(err, user){
                if(!user)return res.status(400).send({msg: 'No user with this name was found'});
                if(user.verified) return res.redirect('/user');
                user.verified = true;
                user.save((err)=>{
                    if(err) res.status(500).send({msg:err.message});
                    res.redirect('/')
                });
            });
        });
    }
};