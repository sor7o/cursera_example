const User = require('../../models/user');
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const Token = require('../../models/token');
const { set } = require('../../mailer/mailer');

module.exports = {
    authenticate: function (req, res, next) {
        const { email, password } = req.body;
        User.findOne({ email: email }, function (err, user) {
            if (err) {
                next(err);
            } else {
                if (user == null) return res.status(401).json({ status: "error", message: "Credentials incorrects", data: null });
                if (user != null && bcrypt.compareSync(password, user.password)) {
                    const token = jwt.sign({ id: user._id }, req.app.get('secretKey'), { expiresIn: '5d' });
                    res.status(200).json({ message: 'User found', data: { user: user, token: token } });
                } else {
                    res.status(401).json({ status: "error", message: 'Credentials incorrects', data: null });
                }
            }
        })
    },
    forgotPassword: function (req, res, next) {
        const { email } = req.body;
        User.findOne({ email: email }, function (err, user) {
            if (!user) return res.status(401).json({ message: "Credentials incorrect or user not exist", data: null });
            user.reset_password_api(function (err) {
                if (err) { return next(err) };
                res.status(200).json({ message: "We have sent you a message to your email to recover your password", data: null })
            })
        });
    },
    reset_password: function (req, res, next) {
        Token.findOne({ token: req.params.token }, function (err, token) {
            if (!token) res.status(400).send({ type: 'no-verified', message: "Your token not exist or is invalid.Verify your token does not expired", data: null });
            User.findById(token._userId, function (err, user) {
                if (!user) return res.status(400).send({ msg: "No user exist asociate with this token" });
                res.render('login/password_reset')
            })
        })
    },
    update_password: function (req, res, next) {
        const { email, password, confirm_password } = req.body;

        /* console.log(tok); */
        if (password == confirm_password) {
            User.findOne({ email: email }, function (err, success) {
                if (err) res.status(400).json({ message: "Email not exist" });
                Token.findOne({ _userId: success._id }, function (err, token_found) {
                    if(err) res.status(204).json({message:"Not found"})
                    User.updateOne({_id: success._id}, {$set: {password: password}},{new:true}, function(err, success_update){
                        if(err) res.status(404).json({message: "Not found"});
                        res.status(200).json(success_update)
                    })
                })
            })
        }else{
            res.status(409).json({message: "Does not the same password"});
        }
    },
    authFacebookToken: function(req, res, next){
        if(req.user){
            req.user.save().then(()=>{
                const token = jwt.sign({id: req.user._id}, req.app.get('secretKey'), {expiresIn: '7d'});
                res.status(200).json({message: "User found or created", data: {user: req.user, token: token}});
            }).catch((err)=>{
                res.status(500).json({message: err.message});
            });
        }else{
            res.status(401);
        }
    }
}