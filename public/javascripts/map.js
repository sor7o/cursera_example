var mymap = L.map('main_map').setView([14.493176, -87.9873314], 15);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoic29yN28iLCJhIjoiY2tmNHZydHQ0MGd2dzMwbnQ2ODEwemFpZiJ9.Til0UT6I_P61I43X8FF3Ww'
}).addTo(mymap);

/* L.marker([14.493176, -87.9873314]).addTo(mymap);
L.marker([14.493721, -87.985215]).addTo(mymap);
L.marker([14.494802, -87.986848]).addTo(mymap); */

$.ajax({
    dataType: "json",
    url: "api/bikes",
    success: function(result){
        result.bikes.forEach(function(bike){
            console.log(result);
            L.marker(bike.ubication, {title: bike.id}).addTo(mymap);
        });
    }
});