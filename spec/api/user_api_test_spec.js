const mongoose = require('mongoose');
const User = require('../../models/user');
const Reserve = require('../../models/reserve');
const Bike = require('../../models/bikes');
const server = require('../../bin/www');
const request = require('request');
const { base } = require('../../models/bikes');

const url_base = "http://localhost:3000/api/user";

describe('USER API', () => {
    beforeEach(function (done) {
        mongoose.connection.close().then(() => {
            const mongoDB = 'mongodb://localhost/red_bikes';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function () {
                console.log('Connected to database');
                done();
            });
        });
    });
    afterEach(function (done) {
        User.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            Reserve.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Bike.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('GET / ', () => {
        it('List all user Status 200', (done) => {
            request.get(url_base, function (error, response, body) {
                const result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.user.length).toBe(0);
                done();
            });
        });
    });

    describe('POST user /create', () => {
        it('Status 200', (done) => {
            const header = { 'content-type': 'application/json' };
            const newUser = '{"name" : "Angel R"}'

            request.post({
                headers: header,
                url: url_base + '/create',
                body: newUser
            }, function (errr, response, body) {
                expect(response.statusCode).toBe(200);
                const result = JSON.parse(body)
                expect(result.name).toBe('Angel R');
                done();
            });
        });
    });

    describe('Reserves /reserve', () => {
        it('Return the information of the reserve', (done) => {
            const user = new User({ "name": "Osmin" });
            user.save();

            const bike = new Bike({ "codigo": 3, "color": "red", "model": "urban" });
            bike.save();

            const header = { 'content-type': 'application/json' };

            const reserve = {
                from: '2020-10-15',
                to: '2020-10-18',
                id: user._id,
                bike_id: bike._id
            };
            request.post({
                headers: header,
                url: url_base + '/reserve',
                body: JSON.stringify(reserve)
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                Reserve.find({}).exec(function (err, success) {
                    if (err) console.log(err);
                    expect(success[0].user).toEqual(user._id)
                    expect(success[0].bike).toEqual(bike._id);
                    done();
                });
            });
        });
    });

    describe('UPDATE user  /update', () => {
        it('status 200', (done) => {
            const user = new User({ "name": "Angel S R" });
            user.save();

            const bike = new Bike({ "codigo": 8, "color": "red", "model": "urban" });
            bike.save();

            User.findById(user._id, function(err, success_user){
                if(err)console.log(err);

                const newUser = {
                    _id: user._id,
                    name: "Angel"
                };

                const header = {'content-type': 'application/json'};
                const update_user = JSON.stringify(newUser);
                User.find({}).exec(function(err, success){
                    console.log(success);
                });
                request.post({
                    headers:header,
                    url: url_base+ '/update_user',
                    body: update_user
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    const result = JSON.parse(body);
                    expect(result.ok).toBe(1);
                    User.find({}).exec(function(err, find_success){
                        if(err)console.log(err);
                        console.log(find_success);
                        expect(find_success.name).toEqual(update_user.name)
                        done();
                    })
                })
            })
        })
    })
});
