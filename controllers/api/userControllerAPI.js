const User = require('../../models/user');

exports.user_list = function (req, res) {
    User.find({}, function (err, user) {
        res.status(200).json({
            user: user
        });
    });
};

exports.user_create = function (req, res) {
    const { name, email, password, password_confirm } = req.body;
    if (password == confirm - password_confirm) {
        const user = new User({ name: name, email: email, password, password });
        user.save(function (err) {
            res.status(200).json(user);
        });
    }else{
        res.status(204).json({message: "Does not the same password"})
    }

};

exports.user_reserve = function (req, res) {
    User.findById(req.body.id, function (err, user) {
        user.reserves(req.body.bike_id, req.body.from, req.body.to, function (err) {
            res.status(200).send();
        })
    })
};
exports.update_reserve = function (req, res) {
    const user = req.body;
    const id = user.id;
    const name_user = user.name;

    User.findById(id, function (err, success) {
        User.updateOne({ _id: success._id }, { $set: { name: name_user } }, { new: true }, function (err, success_update) {
            if (err) console.log(err);
            res.status(200).json(success_update);
        });
    });
};

exports.update_user = function (req, res) {
    const user = req.body;
    const id = user._id;
    const name_user = user.name;

    User.findById(id, function (err, user_existing) {
        if (err) console.log(err);

        User.updateOne({ _id: user._id }, { $set: { name: name_user } }, { new: true }, function (err, success_update) {
            res.status(200).json(success_update);
        });
    });
};