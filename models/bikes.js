const mongoose = require('mongoose');
const schema = mongoose.Schema;

const bikeSchema = new schema({
    codigo: Number,
    color: String,
    model: String,
    ubication:{
        type: [Number], index:{type:'2dsphere', sparse:true}
    }
});

bikeSchema.statics.createInstance= function(codigo, color, model, ubication){
    return new this({
        codigo: codigo,
        color:color,
        model: model,
        ubication: ubication
    })
};

bikeSchema.statics.updateInstance= function(codigo, color, model, ubication){
    this.findOne({codigo: codigo}, function(err,bike){
        bike.codigo = newCod;
        bike.color =color;
        bike.model= model;
        bike.save();
    });
};
bikeSchema.statics.add = function(bikes, cb){
    this.create(bikes, cb);
}

bikeSchema.methods.toString = function(){
    return 'codigo' + this.codigo + ' | color  '+ this.color;
};

bikeSchema.statics.allBikes = function(cb){
    return this.find({}, cb);
}

bikeSchema.statics.findByCod = function(codigo, cb){
    return this.findOne({codigo: codigo}, cb);
};

bikeSchema.statics.removeByCod = function(codigo, cb){
    return this.deleteOne({codigo:codigo}, cb);
};

bikeSchema.statics.updateByCod = function(codigo, cb){
    return this.updateOne({codigo:codigo.codigo},{$set:codigo}, cb)
};
module.exports = mongoose.model('Bike', bikeSchema);




