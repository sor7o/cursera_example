const Reserve = require('../../models/reserve');

exports.reserve_list = function(req, res){
    Reserve.find({}, function(err, success){
        if(err)console.log(err);
        res.status(200).json(success);
    });
};

exports.reserve_update = function(req, res){
    const id = req.body._id;
    const {from, to, bike}= req.body;
    console.log(id);
    console.log(from);
    console.log(to);
    console.log(bike);
    Reserve.reserveFindById(id, function(err, success){
        const updateDate = {
            from: from,
            to: to,
            _id: success._id,
            bike_id:bike ,
        };
        console.log(updateDate);
        if(err)console.log(err);
        Reserve.updateOne({_id: success._id}, {$set: updateDate}, function(err, success_update){
            res.status(200).json(success_update);
        });
    });
};