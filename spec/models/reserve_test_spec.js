const mongoose = require('mongoose');
const Reserve = require('../../models/reserve');
const User = require('../../models/user');
const Bike = require('../../models/bikes');
const server = require('../../bin/www');

describe('Testing bicicletas', function () {
    beforeAll(function (done) {
        mongoose.connection.close().then(() => {


            var mongodb = 'mongodb://localhost/red_bikes';
            mongoose.connect(mongodb, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'Error conectando a la base de datos'));
            db.once('open', function () {
                console.log('Conexion exitosa');
                done();
            });
        });
    })

    afterEach(function (done) {
        User.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            Reserve.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Bike.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('GET list reserve', () => {
        it('List reserve existing', (done) => {
            Reserve.reserve(function (err, reserves) {
                expect(reserves.length).toBe(0);
                done();
            });
        });
    });

    describe('POST /update', () => {
        it('update reserve existing', (done) => {
            const user = new User({ name: 'Osmin Sorto' });
            user.save();

            const bike = new Bike({ codigo: 5, color: "red", model: "urban" });
            bike.save();
            const today = new Date();
            const tomorrow = new Date();
            tomorrow.setDate(today.getDate() + 1);

            user.reserves(bike._id, today, tomorrow, function (err, reserved) {
                if (err) console.log(err);
                Reserve.findById(reserved._id).exec(function (err, reserves) {
                    
                    const to = new Date();
                    to.setDate(to.getDate() + 5);
                    
                    const reserve_update ={
                        _id: reserves._id,
                        from: today,
                        to: to,
                        bike: bike._id,
                        user: user._id
                    };
                    Reserve.update_reserve(reserve_update, function(err, success_update){
                        if(err)console.log(err);
                        expect(success_update.ok).toBe(1);
                        done();
                    })
                });
            });
        });
    });
});