const passport = require('passport');
const LocalStrategy = require('passport-local');
const User = require('../models/user');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookStrategy = require('passport-facebook-token');
var FacebookStrategyToken = require("passport-facebook-token");
passport.use(new FacebookStrategyToken({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
}, function (accessToken, refreshToken, profile, done) {
    try {
        User.findOneOrCreateByFacebook(profile, function (err, userFacebook) {
            if (err) console.log('err' + err);
            return done(err, userFacebook);
        })
    } catch (error2) {
        console.log(error2);
        return done(error2, null)
    }
}));

passport.use(new LocalStrategy({

    usernameField: 'email',
    passwordField: 'password',
}, async (email, password, done) => {
    try {
        const user = await User.findOne({ email: email });
        if (!user.validPassword(password)) {
            return done(null, false, { message: 'Credentials incorrect.' });
        }
        return done(null, user);
    } catch (error) {
        return done(null, false);
    }
}));


passport.use(new GoogleStrategy({
    clientID: process.env.IDCUSTOMER,
    clientSecret: process.env.SECRETCUSTOMER,
    callbackURL: process.env.HOST + '/auth/google/callback',
    profileFields: ['id', 'emeil', 'profile'],
    passReqToCallback: true
},
    function (request, accessToken, refreshToken, profile, done) {
        console.log(profile);
        /*console.log(accessToken); */
        console.log('profile de ' + profile);
        console.log(profile.password);
        User.findOneOrCreateByGoogle(profile, function (err, user) {
            return done(err, user);
        });
    }
));
passport.serializeUser(function (user, cb) {
    cb(null, user.id)
});

passport.deserializeUser(function (id, cb) {
    User.findById(id, function (err, user) {
        cb(null, user);
    })
});

module.exports = passport;