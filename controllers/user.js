const User = require('../models/user');

module.exports ={
    list: function(req, res, next){
        User.find({}, function(err, user){
            console.log(user);
            console.log(err);
            res.render('user/index', {users: user})
        });
    },
    update_get : function(req, res, next){
        User.findById(req.params.id, function(err,user){
            res.render('user/update',{errors:{}, user:user});
        });
    },
    update: function(req, res, next){
        const update_value = {name: req.body.name};
        console.log(update_value);
        User.findByIdAndUpdate(req.params.id, update_value, (err, user)=>{
            if(err){
                console.log(err);
                res.render('user/update',{errors: err.errors, user: new User({name: req.body.name, email:req.body.email})}); 
            }else{
                res.redirect('/user');
                return;
            }
        });
    },

    create_get: function(req, res, next){
        res.render('user/create',{errors:{}, user: new User()});
    },

    create: function(req, res, next){
        console.log(req.body);
        if(req.body.password!= req.body.confirm_password){
            res.render('user/create',{errors: {confirm_password: {message: 'No is the same password'}}, user: new User({name: req.body.name, email: req.body.email})});
            return;
        }
        User.create({name: req.body.name, email: req.body.email, password: req.body.password}, function(err, success_create){
            if(err){
                console.log(err);
                res.render('user/create',{errors: err.errors, user: new User({name: req.body.name, email: req.body.email})});            
            }
            else{
                success_create.send_email_welcome();
                res.redirect('/user');
            }
        });
    },
    delete: function(req, res, next){
        User.findByIdAndDelete(req.body.id, function(err){
            if(err){
                next(err)
            }
            else{
                res.redirect('/user');
            }
        });
    }
}