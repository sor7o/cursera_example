const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const reserveSchema = new Schema({
    from: Date,
    to: Date,
    bike: {type: mongoose.Schema.Types.ObjectId, ref: 'Bikes'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
});

reserveSchema.methods.dayReserve = function(){
    return moment(this.from).diff(moment(this.from), 'days') + 1; 
};
reserveSchema.statics.update_reserve = function(obj, cb){
    this.updateOne({_id: obj._id},{$set: obj}, cb);
};

reserveSchema.statics.reserve = function(cb){
    return this.find({},cb);
};
reserveSchema.statics.reserveFindById= function(id, cb){
    return this.findOne({_id: id}, cb)
}
module.exports = mongoose.model('Reserve', reserveSchema);