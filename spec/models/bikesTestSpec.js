const mongoose = require('mongoose');
const Bikes = require('../../models/bikes');
const server = require('../../bin/www');

describe('Testing bicicletas', function () {
    beforeAll(function (done) {
        mongoose.connection.close().then(() => {


            var mongodb = 'mongodb://localhost/red_bikes';
            mongoose.connect(mongodb, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex:true });

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'Error conectando a la base de datos'));
            db.once('open', function () {
                console.log('Conexion exitosa');
                done();
            });
        });
    })

    afterEach(function (done) {
        Bikes.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bike.CreateInstance', () => {
        it('create a instance', () => {
            const bike = Bikes.createInstance(1, "green", "urban", [-34.5, -54.1]);

            expect(bike.codigo).toBe(1);
            expect(bike.color).toBe("green");
            expect(bike.model).toBe("urban");
            expect(bike.ubication[0]).toBe(-34.5);
            expect(bike.ubication[1]).toBe(-54.1)

        });
    });
    describe('Bikes.Get', () => {
        it('Begin empty', (done) => {
            Bikes.allBikes(function (err, bikes) {
                expect(bikes.length).toBe(0);
                done();
            });

        });
    });
    describe('Bike.add', () => {
        it('Add one bike', (done) => {
            Bikes.allBikes(function (err, bikes) {
                if (err) console.log(err);
                expect(bikes.length).toBe(0);
                const bike1 = new Bikes({ codigo: 1, color: "green", model: "urban" });
                Bikes.add(bike1, function (err, newBike) {
                    if (err) console.log(err);
                    Bikes.allBikes(function (err, bike) {
                        if (err) console.log(err);
                        expect(bike.length).toEqual(1);
                        expect(bike[0].cod).toEqual(bike1.cod);
                        done();
                    });
                });
            });
        });
    });
    describe('Bike.FindOne', () => {
        it('Find one bike by code', (done) => {
            Bikes.allBikes(function (err, bikes) {
                if (err) console.log(err);
                expect(bikes.length).toBe(0);

                const bike1 = new Bikes({ codigo: 1, color: "green", model: "urban" });
                Bikes.add(bike1, function (err, newBike) {
                    if (err) console.log(err);
                    Bikes.findByCod(1, function (err, targetBike) {
                        expect(targetBike.cod).toBe(bike1.cod);
                        expect(targetBike.color).toBe(bike1.color);
                        expect(targetBike.model).toBe(bike1.model);
                        done();
                    });
                });
            });
        });
        describe('Bike.RemoveByCod', () => {
            it('Remove one bike by cod', (done) => {
                Bikes.allBikes(function (err, bike) {
                    if (err) console.log(err);
                    expect(bike.length).toBe(0);

                    const bike1 = new Bikes({ codigo: 2, color: 'red', model: 'urban' });
                    const bike2 = new Bikes({ codigo: 3, color: 'green', model: 'urban' });
                    Bikes.add(bike1, function (err, newBike) {
                        if (err) console.log(err);
                        Bikes.add(bike2, function (err, bike) {
                            if (err) console.log(err);
                            Bikes.allBikes(function (err, bike) {
                                if (err) console.log(err);
                                expect(bike.length).toBe(2)
                                Bikes.removeByCod(2, function (err, rbike) {
                                    if (err) console.log(err);
                                    Bikes.allBikes(function (err, rbike) {
                                        if (err) console.log(err);
                                        expect(rbike.length).toBe(1);
                                        expect(rbike[0].color).toBe(bike2.color);
                                        expect(rbike[0].model).toBe(bike2.model);
                                        done();
                                    })
                                })

                            })
                        })
                    })
                })
            })
        })
    });
});

/* var Bikes = require('../../models/bikes');

beforeEach(()=>{
Bikes.allBikes=[];
});
describe('Bikes.allBikes', ()=>{
    it('Begin empty', ()=>{
        expect(Bikes.allBikes.length).toBe(0)
    });
});

describe('Bikes.add',()=>{
    it('add one',()=>{
        expect(Bikes.allBikes.length).toBe(0)

        var a= new Bikes(1, 'red', 'trek', [14.493721, -87.985215])
        Bikes.add(a);
        expect(Bikes.allBikes.length).toBe(1)
        expect(Bikes.allBikes[0]).toBe(a)
    });
});

describe('Bikes.findById',()=>{
    it('Back bike with id 1',()=>{
        expect(Bikes.allBikes.length).toBe(0)
        var aBike1 = new Bikes(1, "grey", "urban");
        var aBike2 = new Bikes(2, "red", "montain");
        Bikes.add(aBike1);
        Bikes.add(aBike2);
        var targetBike= Bikes.findById(1);

        expect(targetBike.id).toBe(1);
        expect(targetBike.color).toBe(aBike1.color);
        expect(targetBike.model).toBe(aBike1.model);

    })
});

describe('Bikes.removeById', ()=>{
    it('Remove bike by id',()=>{
        expect(Bikes.allBikes.length).toBe(0)
        var aBike1 = new Bikes(1, "grey", "urban");
        Bikes.add(aBike1);

        var targetBike = Bikes.findById(1)
        expect(targetBike.id).toBe(1);


    });
}); */