const passport = require('passport');
const User = require('../models/user');
const Token = require('../models/token');
exports.login = function (req, res, next) {
  res.render('login/login');
};
exports.post_login = function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    console.log(user);
    if (err) return next(err);
    if (!user) return res.render('login/login', { info });
    req.logIn(user, function (err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
};

exports.reset_pass = function (req, res, next) {
  res.render('login/reset_password');
}
exports.reset_password = function (req, res, next) {
  const { email_reset } = req.body;
  console.log(email_reset);
  User.findOne({email:email_reset}, function(err, user){
    console.log(user);
    if(err) return next(err);
    if(!user) return res.render('login/reset_password',{message: 'This email not exist'});
    user.reset_password(function(err){
      if(err)return next(err);
    });
    res.render('login/email_correct');

  })
};

exports.pass_reset = function (req, res, next) {
  console.log("adl");
  Token.findOne({token: req.params.token}, function(err, token){
    if(!token) return res.render('login/reset_password', {message: "Token not exist"});

    User.findById(token._userId, function(err, userToken){
      if(err) return res.render('login/reset_password',{message: "Not exist a user with this token"});
      res.render('login/password_reset');
    })

  })
}
exports.password_reset = function (req, res, nxet) {
  const { password, confirm_password, email } = req.body;
  if(password != confirm_password){
    res.render('login/password_reset');
    return;
  }
  User.findOne({email: email}, function(err, user){
    console.log(user);
    user.password = password;
    user.save(function(err){
      if(err){
        res.render('login/reset_password', {errors: err.erros, user: new User()})
      }
      else{
        res.redirect('/login')
      }
    })
  })
};

exports.logout= function(req, res, next){
  console.log(req.logout());
  res.redirect('/')
}