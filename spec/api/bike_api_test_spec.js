const mongoose = require('mongoose');
const Bikes = require('../../models/bikes');
const server = require('../../bin/www');
const request = require('request');

const url_base = "http://localhost:3000/api/bikes";

describe('Bikes API', () => {
    beforeEach(function (done) {
        mongoose.connection.close().then(() => {
            const mongoDB = 'mongodb://localhost/red_bikes';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true ,useCreateIndex:true});

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function () {
                console.log('Connected to database');
                done();
            });
        });
    });
    afterEach(function (done) {
        Bikes.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET Bikes /', () => {
        it("Status 200", (done) => {
            request.get(url_base, function (error, response, body) {
                const result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bikes.length).toBe(0);
                done();
            });
        });
    });
    describe('POST Bikes /create', () => {
        it('Status code 200', (done) => {
            const header = { 'content-type': 'application/json' };
            const bike1 = '{"codigo": 5, "color": "red", "model": "urban", "latitude": 14.493253, "longitude": -87.986049}';
            request.post({
                headers: header,
                url: url_base + '/create',
                body: bike1
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                const aBike = JSON.parse(body);
                expect(aBike.bikes.color).toBe("red");
                expect(aBike.bikes.ubication[0]).toBe(14.493253);
                expect(aBike.bikes.ubication[1]).toBe(-87.986049);
                done();
            })

        });

    });
    describe('DElETE Bikes /Delete', () => {
        it('Delete bike by Id. Status 204', (done) => {
            const header = { 'content-type': 'application/json' };
            const nBike = '{"codigo": 6, "color": "green", "model":"urban", "latitude": 14.493253, "longitude": -87.986049}';
            const cod_bike_id = '{"codigo": 6}';
            request.post({
                headers: header,
                url: url_base + '/create',
                body: nBike
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                const aBike = JSON.parse(body);
                expect(aBike.bikes.codigo).toBe(6);
                request.delete({
                    headers: header,
                    url: url_base + '/delete',
                    body: cod_bike_id
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(204);
                    done();
                })

            });

        });
    });
    describe('UPDATE Bike /update', () => {
        it('Update by cod', (done) => {
            const header = { 'content-type': 'application/json' };
            const nBike = '{"codigo": 6, "color": "green", "model":"urban", "latitude": 14.493253, "longitude": -87.986049}';
            const nBikeUpdate = '{"codigo": 6, "color": "black", "model":"urban", "latitude": 14.493253, "longitude": -87.986049}';
            request.post({
                headers: header,
                url: url_base + '/create',
                body: nBike
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                const aBike = JSON.parse(body);
                expect(aBike.bikes.codigo).toBe(6);
                request.post({
                    headers: header,
                    url: url_base + '/update',
                    body: nBikeUpdate
                }, function (error, response, body) {
                    const newBike = JSON.parse(body);
                    expect(response.statusCode).toBe(200);
                    expect(newBike.ok).toBe(1);
                    done();
                })

            })
        })
    });
});