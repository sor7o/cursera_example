var Bikes = require('../../models/bikes');

exports.bike_list = function (req, res) {
    Bikes.allBikes(function (err, success) {
        res.status(200).json({
            bikes: success
        });
    });
};


exports.createBike = function (req, res) {
    var aBike = new Bikes({ codigo: req.body.codigo, color: req.body.color, model: req.body.model, ubication: [req.body.latitude, req.body.longitude] })
    Bikes.add(aBike, function (error, success) {
        res.status(200).json({
            bikes: success
        });
    });


};


exports.deleteBike = function (req, res) {
    Bikes.removeByCod(req.body.codigo, function (err, success) {
        if (err) console.log(err);
        res.status(204).send();
    });


};

exports.updateBike = function (req, res) {
    console.log(req.body);
    const { codigo, color, model, latitude, longitude } = req.body;
    Bikes.findByCod(codigo, function (err, success) {
        console.log(success);
        if (err) console.log(err);
        const update_Bike = {
            codigo: success.codigo,
            color: color,
            model: model
        };
        update_Bike.ubication = [latitude, longitude];
        Bikes.updateOne(update_Bike, function (err, success) {
            if(err) console.log(err);
            res.status(200).json(success);
        })
    });
};