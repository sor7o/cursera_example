const mongoose = require('mongoose');
const Reserve = require('./reserve');
const Token = require('./token');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRound = 10;
const uniqueValidator = require('mongoose-unique-validator');
const mailer = require('../mailer/mailer');/* 
const user = require('../controllers/user'); */


const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

}

const userSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'The name is necesary']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'The email is necesary '],
        lowercase: true,
        unique: true,
        validate: [
            validateEmail, 'Please, insert email valid'
        ],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'Insert your password']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verified: {
        type: Boolean,
        default: false
    },
    googleId: {
        type: String
    },
    facebookId:{
        type:String
    }
});
userSchema.plugin(uniqueValidator, { message: 'The {PATH} is already in use' });

userSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt
            .hashSync(this.password, saltRound);
    };
    next();
});

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.reserves = function (bikeId, from, to, cb) {
    const reserve = new Reserve({ user: this._id, bike: bikeId, from: from, to: to });
    console.log(reserve);
    reserve.save(cb);
};

userSchema.methods.send_email_welcome = function (cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;

    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }
        const mailOptions = {
            from: 'no-reply@red_bikes.com',
            to: email_destination,
            subject: 'Verified account',
            text: 'Hello. \n\n Verified your account make click in this link \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '\n'
        }
        mailer.sendMail(mailOptions, (err) => {
            if (err) { return console.log(err.message); }
            console.log('a verification email has been sent to' + email_destination);
        });
    });
};

userSchema.methods.reset_password = function (cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;
    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }
        const mailOptions = {
            from: 'no-reply@red_bikes.com',
            to: email_destination,
            subject: 'Reset password',
            text: 'Hello. \n\n' + 'Verified your account make click in this link \n' + 'http://localhost:3000' + '\/login/password_reset\/' + token.token + '\n',
            html: 'p Hello. \n\n' + 'Verified your account make click in this link \n' + 'http://localhost:3000' + '\/login/password_reset\/' + token.token + '\n'
        }
        mailer.sendMail(mailOptions, (err) => {
            if (err) { return console.log(err.message); }
            console.log('a reset password has been sent to: ' + email_destination);
        });
        cb(null);
    });
}
userSchema.statics.findOneOrCreateByFacebook= function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [{
            'facebookId': condition.id
        },
        {
            'email': condition.emails[0].value,
        },],
    }, (err, result) => {
        if (result) {
            console.log('This result '+result );
            callback(err, result);
        } else {
            console.log("----condition-----------");
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            console.log(values.email);
            values.name = condition.displayName || "Not name";
            values.verified = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('---------------values-----------------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err) };
                callback(err, result)
            })
        }
    }
    )
};
userSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    console.log('self'+self);
    console.log("this password"+condition.password);
    self.findOne({
        $or: [{
            'googleId': condition.id
        },
        {
            'email': condition.emails[0].value
        }]
    }, (err, result) => {
        if (result) {
            console.log(result + ' Resil;sadmas');
            callback(err, result);
        } else {
            console.log("----condition-----------");
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            console.log(values.email);
            values.name = condition.displayName || "Not name";
            values.verified = true;
            values.password = bcrypt.hashSync(condition.id, saltRound);
            console.log('---------------values-----------------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err) };
                callback(err, result)
            })
        }
    }
    )
};

userSchema.methods.reset_password_api = function (cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;
    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }
        const mailOptions = {
            from: 'no-reply@red_bikes.com',
            to: email_destination,
            subject: 'Reset password',
            text: 'Hello. \n\n' + 'Verified your account make click in this link \n' + 'http://localhost:3000' + '\/api/auth/reset\/' + token.token + '\n'
        }
        mailer.sendMail(mailOptions, (err) => {
            if (err) { return console.log(err.message); }
            console.log('a reset password has been sent to: ' + email_destination);
        });
        cb(null);
    });
}

userSchema.statics.allUser = function (cb) {
    return this.find({}, cb);
};

module.exports = mongoose.model('User', userSchema);