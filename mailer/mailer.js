const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
let mailConfig;
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDRID_API_KEY
        }
    }
    mailConfig=sgTransport(options);
} else {
    if (process.env.NODE_ENV === 'production') {
        console.log('xsx');
        const options = {
            auth: {
                api_key: process.env.SENDRID_API_KEY
            }
        }
        mailConfig=sgTransport(options);
    }
    else {
        mailConfig = {
            host: process.env.HOST,
            port: process.env.PORT,
            auth: {
                user: process.env.USER,
                pass: process.env.PASS
            }
        };

    }
}

module.exports = nodemailer.createTransport(mailConfig);