var express = require('express');
var router = express.Router();
var bikesControllers = require("../../controllers/api/bikeControllersAPI");

router.get('/', bikesControllers.bike_list);

router.post('/create', bikesControllers.createBike);

router.delete('/delete',bikesControllers.deleteBike);

router.post('/update', bikesControllers.updateBike);

module.exports = router;