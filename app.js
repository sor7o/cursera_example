require('dotenv').config();
/* require('newrelic'); */
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var moongose = require('mongoose');
var passport = require('./config/passport');
var session = require('express-session');
var bodyParse = require('body-parser');
var jwt = require('jsonwebtoken');
const MongoDBStore = require('connect-mongodb-session')(session);
var User = require('./models/user');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bikesRouter = require('./routes/bikes');
var bikesAPIRouter = require('./routes/api/bikes');
var userAPIRouter = require('./routes/api/user');
var reserveAPIRouter = require('./routes/api/reserve');
var tokenRouter = require('./routes/token');
var sessionRouter = require('./routes/session');
var authAPIRouter = require('./routes/api/auth');
const { assert } = require('console');

let store;
if (process.env.NODE_ENV === 'development') {
  var mongoDB = process.env.MONGO_URI;
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions"
  });
  store.on('error', function (error) {
    assert.ifError(error);
    assert.ok(false)
  });
}

var app = express();
app.set('secretKey', 'jwt_pwd_!!aa!!a-z_!!1234567890')
app.use(session({
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bikes_!!_**!!"**"!!12314'
}));



var mongoDB = process.env.MONGO_URI;
moongose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
moongose.Promise = global.Promise;
var db = moongose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'));
db.once('open', function () {
  console.log('Connected to database MongoDB');
});



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.static("public"))
app.use(express.urlencoded({ extended: false }));
app.use(bodyParse.urlencoded({ extended: false }))
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.get('/email', function (req, res) {
  res.render('login/email_correct')
});
app.use('/privacy_policy.html', function (req, res) {
  res.sendFile('public/privacy_policy');
});
app.use('/google1b42a450b2bdeb59.html', function (req, res) {
  res.sendFile('public/google1b42a450b2bdeb59.html')
});

app.get('/auth/google',
  passport.authenticate('google', {
    scope:
      ['profile',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/plus.login',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/gmail.readonly',
        'https://www.googleapis.com/auth/plus.profile.emails.read'
      ]
  }
  ));

app.get('/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/login'
  }),
  function (req, res) {
    res.redirect('/');
  }
);
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/bikes', loggedIn, bikesRouter);
app.use('/api/bikes', validUser, bikesAPIRouter);
app.use('/api/user', userAPIRouter);
app.use('/api/reserve', reserveAPIRouter);
app.use('/token', tokenRouter);
app.use('/login', sessionRouter);
app.use('/api/auth', authAPIRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  }
  else {
    res.redirect('/login')
  }
};

function validUser(req, res, next) {
  console.log('jwd');
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({ status: "error", message: err.message, data: null });
    } else {
      req.body.userId = decoded.id;

      console.log('jwt verify' + decoded);
      next();
    }
  })
}

module.exports = app;
